import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';

import '../config/theme.dart';


void showSuccessToast({required String text}) {
  Fluttertoast.showToast(
      msg: text,
      toastLength: Toast.LENGTH_LONG,
      gravity: ToastGravity.BOTTOM,
      timeInSecForIosWeb: 1,
      backgroundColor: MyColors.successColor,
      textColor: Colors.white,
      fontSize: 16.0);
}

void showErrorToast({required String text}) {
  Fluttertoast.showToast(
      msg: text,
      toastLength: Toast.LENGTH_LONG,
      gravity: ToastGravity.BOTTOM,
      timeInSecForIosWeb: 1,
      backgroundColor: MyColors.errorColor,
      textColor: Colors.white,
      fontSize: 16.0);
}

void showWarringToast({required String text}) {
  Fluttertoast.showToast(
      msg: text,
      toastLength: Toast.LENGTH_LONG,
      gravity: ToastGravity.BOTTOM,
      timeInSecForIosWeb: 1,
      backgroundColor: MyColors.warringColor,
      textColor: Colors.white,
      fontSize: 16.0);
}