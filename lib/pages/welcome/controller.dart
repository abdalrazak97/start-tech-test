import 'package:get/get.dart';
import 'package:start_tech/routers/app_routers.dart';

class WelcomeController extends GetxController {
  goToLoginPage() {
    Get.toNamed(AppRoutes.login);
  }

  goToRegisterPage() {
    Get.toNamed(AppRoutes.register);
  }

  @override
  void onReady() {
    // TODO: implement onReady
    super.onReady();
  }

  @override
  void onClose() {
    // TODO: implement onClose
    super.onClose();
  }
}
