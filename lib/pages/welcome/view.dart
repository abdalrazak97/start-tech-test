import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:get/get.dart';
import '../../components/appBar/empty_appBar.dart';
import '../../components/buttons/rect_button.dart';
import 'controller.dart';

class WelcomePage extends GetView<WelcomeController> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Theme.of(context).colorScheme.background,
      appBar: EmptyAppBar(),
      body: Center(
        child: Column(
          children: [
            SizedBox(height: 50.h),
            Image.asset(
              "assets/images/ali_fouad_logo.png",
              width: 100.w,
              height: 150.h,
            ),
            Text("Welcome to the app",
                style: Theme.of(context).textTheme.displayLarge?.copyWith(
                      fontSize: 24.sp,
                    )),
            Spacer(),
            Column(
              children: [
                RectButton(
                  title: "Login",
                  fontSize: 18.sp,
                  width: 0.7.sw,
                  textColor: Colors.white,
                  onPressed: () => controller.goToLoginPage(),
                ),
                SizedBox(height: 10.h),
                RectButton(
                  title: "Register",
                  width: 0.7.sw,
                  fontSize: 18.sp,
                  backgroundColor: Colors.white,
                  borderColor: Theme.of(context).primaryColor,
                  textColor: Theme.of(context).primaryColor,
                  onPressed: () => controller.goToRegisterPage(),
                ),
              ],
            ),
            Spacer(),
            RichText(
              textAlign: TextAlign.center,
              text: TextSpan(children: [
                TextSpan(
                  text: "Designed & Developed by ",
                  style: Theme.of(context).textTheme.displayMedium,
                ),
                TextSpan(
                  text: "Abdulrazzaq ",
                  style: Theme.of(context).textTheme.displayMedium?.copyWith(
                        decoration: TextDecoration.underline,
                        fontWeight: FontWeight.w600,
                      ),
                ),
              ]),
            ),
            SizedBox(height: 20.h),
          ],
        ),
      ),
    );
  }
}
