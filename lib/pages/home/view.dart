import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:get/get.dart';
import 'package:modal_progress_hud_nsn/modal_progress_hud_nsn.dart';
import 'package:start_tech/globals.dart';

import '../../components/cell/cell_widget.dart';
import 'controller.dart';

class HomePage extends GetView<HomeController> {
  const HomePage({super.key});

  @override
  Widget build(BuildContext context) {
    return GetBuilder<HomeController>(
        init: controller,
        builder: (logic) {
          return Scaffold(
            appBar: AppBar(
              title: Text(
                "Home Page",
                style: Theme.of(context).textTheme.displayLarge?.copyWith(
                      color: Colors.white,
                    ),
              ),
              centerTitle: true,
              backgroundColor: Theme.of(context).primaryColor,
            ),
            backgroundColor: Theme.of(context).colorScheme.background,
            body: ModalProgressHUD(
              inAsyncCall: controller.isLoading.value,
              progressIndicator: CircularProgressIndicator(color: Theme.of(context).primaryColor),
              child: Column(children: [
                Container(
                  margin: EdgeInsets.symmetric(horizontal: 20.w, vertical: 25.h),
                  child: Column(
                    children: [
                      Row(
                        children: [
                          Icon(Icons.perm_identity_sharp, size: 25.r, color: Theme.of(context).primaryColor),
                          SizedBox(width: 10.w),
                          Text(
                            userName,
                            style: Theme.of(context).textTheme.displayLarge?.copyWith(color: Colors.grey),
                          )
                        ],
                      ),
                      SizedBox(height: 10.h),
                      Row(
                        children: [
                          Icon(Icons.phone_android, size: 25.r, color: Theme.of(context).primaryColor),
                          SizedBox(width: 10.w),
                          Text(
                            phone,
                            style: Theme.of(context).textTheme.displayLarge?.copyWith(color: Colors.grey),
                          )
                        ],
                      ),
                      SizedBox(height: 10.h),
                      Row(
                        children: [
                          Icon(Icons.email_outlined, size: 25.r, color: Theme.of(context).primaryColor),
                          SizedBox(width: 10.w),
                          Text(
                            email,
                            style: Theme.of(context).textTheme.displayLarge?.copyWith(color: Colors.grey),
                          )
                        ],
                      ),
                    ],
                  ),
                ),
                CellWidget(title: "Update Information", callback: () => controller.goToUpdateInfo()),
                CellWidget(title: "Change Password", callback: () {}),
                CellWidget(title: "Delete Account", callback: () => controller.deleteAccount(context)),
                CellWidget(title: "Logout ", callback: () => controller.logout(context)),
              ]),
            ),
          );
        });
  }
}
