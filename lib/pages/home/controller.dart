import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:start_tech/components/dialogs/delete_account_dialog_widget.dart';
import 'package:start_tech/routers/app_routers.dart';

import '../../api/delete_account_api.dart';
import '../../components/dialogs/log_out_dialog_widget.dart';
import '../../globals.dart';
import '../../shared_preferences.dart';
import '../../utils/toast_utils.dart';

class HomeController extends GetxController {

  final isLoading= false.obs;
  goToUpdateInfo() async {
    await Get.toNamed(AppRoutes.updateInformation);
    update();
  }

  deleteAccount(BuildContext context) {
    showDialog(
        context: context,
        builder: (context) {
          return DeleteAccountDialogWidget(
            callback: () async {
              isLoading.value  =true ;
              update();
              var res = await DeleteAccountApi.deleteAccount();
              if (res.success) {
                isLogin = false;
                id = "";
                userToken = "";
                phone = "";
                email = "";
                userName = "";
                await Preferences.saveIsLoggedFromLocalMemory(isLogin);
                await Preferences.saveUserIdFromLocalMemory(id);
                await Preferences.saveUserTokenFromLocalMemory(userToken);
                await Preferences.saveUserPhoneFromLocalMemory(phone);
                await Preferences.saveUserEmailFromLocalMemory(email);
                await Preferences.saveUserNameFromLocalMemory(userName);

                showSuccessToast(text: "Deleted Successfully");
                Get.offAllNamed(AppRoutes.welcome);
              } else {
                showErrorToast(text: res.message);
              }
              isLoading.value  =false ;
              update();
            },
          );
        });
  }

  logout(BuildContext context) {
    showDialog(
        context: context,
        builder: (context) {
          return LogOutDialogWidget(
            callback: () async {
              isLogin = false;
              id = "";
              userToken = "";
              phone = "";
              email = "";
              userName = "";
              await Preferences.saveIsLoggedFromLocalMemory(isLogin);
              await Preferences.saveUserIdFromLocalMemory(id);
              await Preferences.saveUserTokenFromLocalMemory(userToken);
              await Preferences.saveUserPhoneFromLocalMemory(phone);
              await Preferences.saveUserEmailFromLocalMemory(email);
              await Preferences.saveUserNameFromLocalMemory(userName);
              update();
              showSuccessToast(text: "Logout Successfully");
              Get.offAllNamed(AppRoutes.welcome);
            },
          );
        });
  }

  @override
  void onReady() {
    // TODO: implement onReady
    super.onReady();
  }

  @override
  void onClose() {
    // TODO: implement onClose
    super.onClose();
  }
}
