import 'package:get/get.dart';

import 'controller.dart';

class UpdateInformationBinding extends Bindings {
  @override
  void dependencies() {
    Get.lazyPut(() => UpdateInformationController());
  }
}
