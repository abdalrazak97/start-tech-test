import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:get/get.dart';
import 'package:phone_number/phone_number.dart';
import 'package:start_tech/api/update_information_api.dart';
import 'package:start_tech/globals.dart';

import '../../model/user_model.dart';
import '../../shared_preferences.dart';
import '../../utils/toast_utils.dart';

class UpdateInformationController extends GetxController {
  TextEditingController fullNameController = TextEditingController();
  TextEditingController emailController = TextEditingController();
  TextEditingController phoneController = TextEditingController();
  final selectedCountry = "+971".obs;
  final isLoading = false.obs;
  PhoneNumberUtil phoneUtil = PhoneNumberUtil();
  late UserModelDataModel userModelDataModel;

  void splitMobile(phone) async {
    try {
      PhoneNumber numberProto = await phoneUtil.parse("+$phone");
      selectedCountry.value = "+${numberProto.countryCode}";
      phoneController.text = numberProto.nationalNumber;
      update();
    } catch (e) {
      print(e.toString());
    }
  }

  saveUpdateInfo() async {
    isLoading.value = true;
    update();
    var res = await UpdatedInformationApi.updateInfo(
      email: emailController.text.trim(),
      country_code: selectedCountry.value,
      name: fullNameController.text,
      phone: phoneController.text,
    );
    isLoading.value = false;
    if (res.success) {
      userModelDataModel = UserModelDataModel.fromJson(res.data);

      id = userModelDataModel.id;
      userName = userModelDataModel.name ?? "";
      email = userModelDataModel.email ?? "";
      phone = "${userModelDataModel.country_code} ${userModelDataModel.phone!}";

      await Preferences.saveUserIdFromLocalMemory(id);
      await Preferences.saveUserPhoneFromLocalMemory(phone);
      await Preferences.saveUserEmailFromLocalMemory(email);
      await Preferences.saveUserNameFromLocalMemory(userName);
      Get.back();
      Get.snackbar(
        'Success',
        'your information is updated Successfully',
        colorText: Colors.white,
        backgroundColor: Colors.green,
        snackPosition: SnackPosition.BOTTOM,
      );
    } else {
      showErrorToast(text: res.message);
    }
    update();
  }

  @override
  void onInit() async {
    splitMobile(phone);
    fullNameController.text = userName;
    emailController.text = email;

    super.onInit();
  }

  @override
  void onClose() {
    // TODO: implement onClose
    super.onClose();
  }
}
