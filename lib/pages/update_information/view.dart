import 'package:country_code_picker_x/country_code_picker_x.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:get/get.dart';
import 'package:modal_progress_hud_nsn/modal_progress_hud_nsn.dart';

import '../../components/buttons/rect_button.dart';
import '../../components/textFieldForm/text_form_field.dart';
import 'controller.dart';

class UpdateInformationPage extends GetView<UpdateInformationController> {
  const UpdateInformationPage({super.key});

  @override
  Widget build(BuildContext context) {
    return GetBuilder<UpdateInformationController>(
        init: controller,
        builder: (logic) {
          return Scaffold(
              appBar: AppBar(
                title: Text(
                  "Update Information",
                  style: Theme.of(context).textTheme.displayLarge?.copyWith(
                        color: Colors.white,
                      ),
                ),
                centerTitle: true,
                backgroundColor: Theme.of(context).primaryColor,
              ),
              backgroundColor: Theme.of(context).colorScheme.background,
              body: ModalProgressHUD(
                inAsyncCall: controller.isLoading.value,
                progressIndicator: CircularProgressIndicator(color: Theme.of(context).primaryColor),
                child: Padding(
                  padding: EdgeInsets.symmetric(horizontal: 40.w),
                  child: SingleChildScrollView(
                    child: Column(
                      children: [
                        SizedBox(height: 20.h),
                        TextFormFieldWidget(
                          controller: controller.fullNameController,
                          hintText: "full name",
                          textInputAction: TextInputAction.next,
                        ),
                        SizedBox(height: 10.h),
                        TextFormFieldWidget(
                          controller: controller.phoneController,
                          textInputType: TextInputType.phone,
                          textInputAction: TextInputAction.next,
                          hintText: "5xxxxxxxx",
                          textAlign: TextAlign.start,
                          prefixIcon: Container(
                            height: 40.h,
                            width: 60.w,
                            margin: EdgeInsets.only(left: 10.w, right: 20.w),
                            child: CountryCodePickerX(
                              onChanged: (value) {
                                controller.selectedCountry.value = value.dialCode!;
                              },
                              initialSelection: 'AE',
                              favorite: const ['+971', 'AE'],
                              builder: (value) {
                                return Row(
                                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                  children: [
                                    ClipRRect(
                                      borderRadius: BorderRadius.circular(5.r),
                                      child: Image.asset(
                                        value!.flagUri!,
                                        package: 'country_code_picker_x',
                                        width: 28.w,
                                        height: 22.h,
                                      ),
                                    ),
                                  ],
                                );
                              },
                              showCountryOnly: false,
                              showFlag: true,
                              showFlagMain: true,
                              searchStyle: Theme.of(context).textTheme.displayMedium?.copyWith(color: Colors.black),
                              dialogTextStyle: Theme.of(context).textTheme.displayMedium?.copyWith(color: Colors.black),
                              textStyle: Theme.of(context).textTheme.displayMedium?.copyWith(color: Colors.black),
                              hideMainText: true,
                              showOnlyCountryWhenClosed: false,
                              alignLeft: false,
                            ),
                          ),
                        ),
                        SizedBox(height: 10.h),
                        TextFormFieldWidget(
                          controller: controller.emailController,
                          textInputType: TextInputType.emailAddress,
                          textInputAction: TextInputAction.done,
                          hintText: "email@xmail.com",
                        ),
                        SizedBox(height: 10.h),
                        RectButton(
                          title: "Save",
                          fontSize: 18.sp,
                          width: 0.7.sw,
                          textColor: Colors.white,
                          onPressed: () => controller.saveUpdateInfo(),
                        ),
                      ],
                    ),
                  ),
                ),
              ));
        });
  }
}
