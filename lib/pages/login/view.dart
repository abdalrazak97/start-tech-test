import 'package:flutter/gestures.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:get/get.dart';
import 'package:modal_progress_hud_nsn/modal_progress_hud_nsn.dart';

import '../../components/appBar/empty_appBar.dart';
import '../../components/buttons/rect_button.dart';
import '../../components/textFieldForm/text_form_field.dart';
import 'controller.dart';

class LoginPage extends GetView<LoginController> {
  const LoginPage({super.key});

  @override
  Widget build(BuildContext context) {
    return GetBuilder<LoginController>(
        init: controller,
        builder: (logic) {
          return Scaffold(
            backgroundColor: Theme.of(context).colorScheme.background,
            appBar: EmptyAppBar(),
            body: ModalProgressHUD(
              inAsyncCall: controller.isLoading.value,
              progressIndicator: CircularProgressIndicator(color: Theme.of(context).primaryColor),
              child: SingleChildScrollView(
                child: SizedBox(
                  height: 1.sh,
                  child: Column(
                    children: [
                      SizedBox(height: 40.h),
                      Image.asset(
                        "assets/images/ali_fouad_logo.png",
                        width: 100.w,
                        height: 150.h,
                      ),
                      Text("Login",
                          style: Theme.of(context).textTheme.displayLarge?.copyWith(
                                fontSize: 24.sp,
                              )),
                      Spacer(),
                      Padding(
                        padding: EdgeInsets.symmetric(horizontal: 40.w),
                        child: Column(
                          children: [
                            TextFormFieldWidget(
                              controller: controller.emailAddressController,
                              textInputType: TextInputType.emailAddress,
                              hintText: "Email Address",
                              textInputAction: TextInputAction.next,
                            ),
                            SizedBox(height: 10.h),
                            TextFormFieldWidget(
                              controller: controller.passwordController,
                              textInputAction: TextInputAction.done,
                              hintText: "Password",
                              toggleObscured: () => controller.changePasswordToggle(),
                              isPassword: controller.passwordToggle.value,
                              suffix: true,
                            ),
                            SizedBox(height: 10.h),
                            RectButton(
                              title: "Login",
                              fontSize: 18.sp,
                              width: 0.7.sw,
                              textColor: Colors.white,
                              onPressed: () => controller.goToHomePage(),
                            ),
                          ],
                        ),
                      ),
                      Spacer(),
                      RichText(
                        textAlign: TextAlign.center,
                        text: TextSpan(children: [
                          TextSpan(
                            text: "Don't have account? ",
                            style: Theme.of(context).textTheme.displayMedium,
                          ),
                          TextSpan(
                            text: "Register ",
                            recognizer: TapGestureRecognizer()..onTap = () => controller.goToRegisterPage(),
                            style: Theme.of(context).textTheme.displayMedium?.copyWith(
                                  fontWeight: FontWeight.w600,
                                ),
                          ),
                        ]),
                      ),
                      SizedBox(height: MediaQuery.of(context).padding.bottom + 40.h),
                    ],
                  ),
                ),
              ),
            ),
          );
        });
  }
}
