import 'package:flutter/cupertino.dart';
import 'package:get/get.dart';
import 'package:start_tech/config/Http.dart';
import 'package:start_tech/globals.dart';
import 'package:start_tech/model/user_model.dart';
import 'package:start_tech/routers/app_routers.dart';

import '../../api/login_api.dart';
import '../../config/api_interface.dart';
import '../../shared_preferences.dart';
import '../../utils/toast_utils.dart';

class LoginController extends GetxController {
  TextEditingController emailAddressController = TextEditingController();
  TextEditingController passwordController = TextEditingController();
  final passwordToggle = false.obs;
  final isLoading = false.obs;
  late UserModelDataModel userModelDataModel;

  changePasswordToggle() {
    passwordToggle.value = !passwordToggle.value;
    update();
  }

  goToHomePage() async {
    isLoading.value = true;
    update();
    var res = await LoginApi.login(
      email: emailAddressController.text,
      password: passwordController.text,
    );
    isLoading.value = false;
    if (res.success) {
      userModelDataModel = UserModelDataModel.fromJson(res.data);
      isLogin = true;
      userToken = userModelDataModel.token ?? "";
      id = userModelDataModel.id;
      userName = userModelDataModel.name ?? "";
      email = userModelDataModel.email ?? "";
      phone = "${userModelDataModel.country_code} ${userModelDataModel.phone!}";

      await Preferences.saveIsLoggedFromLocalMemory(isLogin);
      await Preferences.saveUserIdFromLocalMemory(id);
      await Preferences.saveUserTokenFromLocalMemory(userToken);
      await Preferences.saveUserPhoneFromLocalMemory(phone);
      await Preferences.saveUserEmailFromLocalMemory(email);
      await Preferences.saveUserNameFromLocalMemory(userName);
      showSuccessToast(text: "Login Successfully");
      Get.offAllNamed(AppRoutes.home);
    } else {
      showErrorToast(text: res.message);
    }
    update();
  }

  goToRegisterPage() {
    Get.toNamed(AppRoutes.register);
  }

  @override
  void onReady() {
    // TODO: implement onReady
    super.onReady();
  }

  @override
  void onClose() {
    // TODO: implement onClose
    super.onClose();
  }
}
