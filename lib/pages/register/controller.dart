import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:phone_number/phone_number.dart';
import 'package:start_tech/api/register_api.dart';
import 'package:start_tech/model/user_model.dart';

import '../../globals.dart';
import '../../routers/app_routers.dart';
import '../../shared_preferences.dart';
import '../../utils/toast_utils.dart';

class RegisterController extends GetxController {
  TextEditingController fullNameController = TextEditingController();
  TextEditingController phoneController = TextEditingController();
  TextEditingController emailAddressController = TextEditingController();
  TextEditingController passwordController = TextEditingController();
  TextEditingController confirmPasswordController = TextEditingController();

  final passwordToggle = false.obs;
  final isLoading = false.obs;
  final confirmPasswordToggle = false.obs;
  final selectedCountry = "+971".obs;
  late UserModelDataModel userModelDataModel;

  changePasswordToggle() {
    passwordToggle.value = !passwordToggle.value;
    update();
  }

  changeConfirmPasswordToggle() {
    confirmPasswordToggle.value = !confirmPasswordToggle.value;
    update();
  }

  register() async {
    if (phoneController.text.isNotEmpty) {
      try {
        if ((!await PhoneNumberUtil().validate("$selectedCountry${phoneController.text}"))) {
        return  showWarringToast(text: "Please enter correct phone number");
        }
      } catch (e) {}
    }
    isLoading.value = true;
    update();
    var res = await RegisterApi.register(
      email: emailAddressController.text.trim(),
      password: passwordController.text,
      password_confirm: confirmPasswordController.text,
      country_code: selectedCountry.value,
      name: fullNameController.text,
      phone: phoneController.text,
    );
    isLoading.value = false;
    if (res.success) {
      userModelDataModel = UserModelDataModel.fromJson(res.data);
      isLogin = true;
      userToken = userModelDataModel.token ?? "";
      id = userModelDataModel.id;
      userName = userModelDataModel.name ?? "";
      email = userModelDataModel.email ?? "";
      phone = "${userModelDataModel.country_code} ${userModelDataModel.phone!}";

      await Preferences.saveIsLoggedFromLocalMemory(isLogin);
      await Preferences.saveUserIdFromLocalMemory(id);
      await Preferences.saveUserTokenFromLocalMemory(userToken);
      await Preferences.saveUserPhoneFromLocalMemory(phone);
      await Preferences.saveUserEmailFromLocalMemory(email);
      await Preferences.saveUserNameFromLocalMemory(userName);
      showSuccessToast(text: "Register Successfully");
      Get.offAllNamed(AppRoutes.home);
    } else {
      showErrorToast(text: res.message);
    }
    update();
  }

  goToLoginPage() {
    Get.toNamed(AppRoutes.login);
  }

  @override
  void onReady() {
    // TODO: implement onReady
    super.onReady();
  }

  @override
  void onClose() {
    // TODO: implement onClose
    super.onClose();
  }
}
