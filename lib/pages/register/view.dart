import 'package:flutter/gestures.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:get/get.dart';
import 'package:modal_progress_hud_nsn/modal_progress_hud_nsn.dart';
import 'package:start_tech/components/appBar/empty_appBar.dart';
import 'package:country_code_picker_x/country_code_picker_x.dart';
import '../../components/buttons/rect_button.dart';
import '../../components/textFieldForm/text_form_field.dart';
import 'controller.dart';

class RegisterPage extends GetView<RegisterController> {
  const RegisterPage({super.key});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Theme.of(context).colorScheme.background,
      appBar: EmptyAppBar(),
      body: GetBuilder<RegisterController>(
          init: controller,
          builder: (logic) {
            return ModalProgressHUD(
              inAsyncCall: controller.isLoading.value,
              progressIndicator: CircularProgressIndicator(color: Theme.of(context).primaryColor),
              child: SingleChildScrollView(
                child: Column(
                  children: [
                    SizedBox(height: 40.h),
                    Image.asset(
                      "assets/images/ali_fouad_logo.png",
                      width: 100.w,
                      height: 150.h,
                    ),
                    Text("Register",
                        style: Theme.of(context).textTheme.displayLarge?.copyWith(
                              fontSize: 24.sp,
                            )),
                    Padding(
                      padding: EdgeInsets.symmetric(horizontal: 40.w,vertical: 10.h),
                      child: Column(
                        children: [
                          TextFormFieldWidget(
                            controller: controller.fullNameController,
                            textInputType: TextInputType.name,
                            textInputAction: TextInputAction.next,
                            hintText: "Full name",
                          ),
                          SizedBox(height: 10.h),
                          TextFormFieldWidget(
                            controller: controller.phoneController,
                            textInputAction: TextInputAction.next,
                            textInputType: TextInputType.phone,
                            hintText: "5xxxxxxxxx",
                            textAlign: TextAlign.start,
                            prefixIcon: Container(
                              height: 40.h,
                              width: 60.w,
                              margin: EdgeInsets.only(left: 10.w, right: 20.w),
                              child: CountryCodePickerX(
                                onChanged: (value) {
                                  controller.selectedCountry.value = value.dialCode!;
                                },
                                initialSelection: 'AE',
                                favorite: const ['+971', 'AE'],
                                builder: (value) {
                                  return Row(
                                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                    children: [
                                      ClipRRect(
                                        borderRadius: BorderRadius.circular(5.r),
                                        child: Image.asset(
                                          value!.flagUri!,
                                          package: 'country_code_picker_x',
                                          width: 28.w,
                                          height: 22.h,
                                        ),
                                      ),
                                    ],
                                  );
                                },
                                showCountryOnly: false,
                                showFlag: true,
                                showFlagMain: true,
                                searchStyle: Theme.of(context).textTheme.displayMedium?.copyWith(color: Colors.black),
                                dialogTextStyle: Theme.of(context).textTheme.displayMedium?.copyWith(color: Colors.black),
                                textStyle: Theme.of(context).textTheme.displayMedium?.copyWith(color: Colors.black),
                                hideMainText: true,
                                showOnlyCountryWhenClosed: false,
                                alignLeft: false,
                              ),
                            ),
                          ),
                          SizedBox(height: 10.h),
                          TextFormFieldWidget(
                            controller: controller.emailAddressController,
                            textInputType: TextInputType.emailAddress,
                            hintText: "Email Address",
                            textInputAction: TextInputAction.next,
                          ),
                          SizedBox(height: 10.h),
                          TextFormFieldWidget(
                            controller: controller.passwordController,
                            textInputType: TextInputType.text,
                            textInputAction: TextInputAction.next,
                            hintText: "Password",
                            toggleObscured: () => controller.changePasswordToggle(),
                            isPassword: controller.passwordToggle.value,
                            suffix: true,
                          ),
                          SizedBox(height: 10.h),
                          TextFormFieldWidget(
                            controller: controller.confirmPasswordController,
                            textInputType: TextInputType.text,
                            textInputAction: TextInputAction.done,
                            hintText: "Confirm Password",
                            toggleObscured: () => controller.changeConfirmPasswordToggle(),
                            isPassword: controller.confirmPasswordToggle.value,
                            suffix: true,
                          ),
                          SizedBox(height: 10.h),
                          RectButton(
                            title: "Register",
                            fontSize: 18.sp,
                            width: 0.7.sw,
                            textColor: Colors.white,
                            onPressed: () => controller.register(),
                          ),
                        ],
                      ),
                    ),
                    RichText(
                      textAlign: TextAlign.center,
                      text: TextSpan(children: [
                        TextSpan(
                          text: "Already have an account? ",
                          style: Theme.of(context).textTheme.displayMedium,
                        ),
                        TextSpan(
                          text: "Login ",
                          recognizer: TapGestureRecognizer()..onTap = () => controller.goToLoginPage(),
                          style: Theme.of(context).textTheme.displayMedium?.copyWith(
                                fontWeight: FontWeight.w600,
                              ),
                        ),
                      ]),
                    ),
                    SizedBox(height: 20.h),
                  ],
                ),
              ),
            );
          }),
    );
  }
}
