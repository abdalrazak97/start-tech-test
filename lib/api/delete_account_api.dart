import 'package:start_tech/globals.dart';

import '../../config/Http.dart';
import '../../config/api_interface.dart';

class DeleteAccountApi {
  static Future<BaseResponse> deleteAccount() async {

    var response = await Http().delete(
      ApiInterface.deleteAccount,
      token: userToken,
    );

    return response;
  }
}
