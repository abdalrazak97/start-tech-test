import '../../config/Http.dart';
import '../../config/api_interface.dart';

class RegisterApi {
  static Future<BaseResponse> register({
    String? name,
    String? country_code,
    String? phone,
    String? email,
    String? password,
    String? password_confirm,
  }) async {
    var param = {
      if (name != null) "name": name,
      if (email != null) "email": email,
      if (phone != null) "phone": phone,
      if (password != null) "password": password,
      if (country_code != null) "country_code": country_code,
      if (password_confirm != null) "password_confirm": password_confirm,
    };

    var response = await Http().post(
      ApiInterface.register,
      param: param,
    );

    return response;
  }
}
