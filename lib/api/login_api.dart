import '../../config/Http.dart';
import '../../config/api_interface.dart';

class LoginApi {
  static Future<BaseResponse> login({
    String? email,
    String? password,
  }) async {
    var param = {
      if (email != null) "email": email,
      if (password != null) "password": password,
    };

    var response = await Http().post(
      ApiInterface.login,
      param: param,
    );

    return response;
  }
}
