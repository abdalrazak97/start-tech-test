import 'package:start_tech/globals.dart';

import '../../config/Http.dart';
import '../../config/api_interface.dart';

class UpdatedInformationApi {
  static Future<BaseResponse> updateInfo({
    String? name,
    String? country_code,
    String? phone,
    String? email,
  }) async {
    var param = {
      if (name != null) "name": name,
      if (email != null) "email": email,
      if (phone != null) "phone": phone,
      if (country_code != null) "country_code": country_code,
    };

    var response = await Http().post(
      ApiInterface.updateInfo,
      param: param,
      token: userToken,
    );

    return response;
  }
}
