import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';

import 'constant.dart';

class Themes {
  static ThemeData lightTheme = ThemeData.light().copyWith(
    primaryColor: const Color(0xff3D0548),
    hintColor: const Color(0xff8696A2),

    colorScheme: const ColorScheme.light().copyWith(
      background: Colors.white,

    ),
    textTheme: TextTheme(
      displayLarge: TextStyle(
        color: const Color(0xff3E0A4E),
        fontWeight: FontWeight.w700,
        fontFamily: "AlexandriaFLF",
        fontSize: 16.sp,
      ),
      displayMedium: TextStyle(
        color: const Color(0xff3E0A4E),
        fontWeight: FontWeight.w400,
        fontFamily: "AlexandriaFLF",
        fontSize: 16.sp,
      ),
      displaySmall: TextStyle(
        color: const Color(0xff3E0A4E),
        fontWeight: FontWeight.w400,
        fontFamily: "AlexandriaFLF",
        fontSize: 16.sp,
      ),
      headlineMedium: TextStyle(
        color: const Color(0xff84949F),
        fontWeight: FontWeight.w400,
        fontFamily: "AlexandriaFLF",
        fontSize: 16.sp,
      ),
    ),
  );
}

class MyColors {
  static const Color errorColor = Color(0xffF26666);
  static const Color warringColor = Color(0xffFBBC05);
  static const Color successColor = Color(0xff3F845F);

 static Color white = const Color(0xffffffff);
 static Color greyTextColor = const Color(0xffB1B1B1);
  static Color greyButtonColor = const Color(0xffEDEDED);
  static Color darkGreyTextColor = const Color(0xff000000).withOpacity(0.5);
  static Color hintColor = const Color(0xffC5C5C5);
  static Color textFieldBackground = const Color(0xffFFFFFF);
  static Color greyOneColor = const Color(0xff333333);

}
