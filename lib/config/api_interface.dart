class ApiInterface {

  static const String apiHost = 'http://testapi.alifouad91.com/api/';

  static const String login = 'login';
  static const String register = 'user/register';
  static const String updateInfo = 'user/update';
  static const String deleteAccount = 'user/delete';


  }
