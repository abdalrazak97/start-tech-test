import 'dart:convert';

import 'package:data_connection_checker_tv/data_connection_checker.dart';
import 'package:dio/dio.dart' as diio;
import 'package:dio/dio.dart';
import 'api_interface.dart';
import 'constant.dart';

class Http {
  factory Http() => _instance;

  static late final Http _instance = Http._internal();

  Dio dio = Dio(BaseOptions(
    baseUrl: ApiInterface.apiHost,
    responseType: ResponseType.plain,
    followRedirects: true,
    validateStatus: (status) {
      return status! < 500;
    },
    receiveTimeout: const Duration(seconds: 10),
    connectTimeout: const Duration(seconds: 10),
  ));
  final DataConnectionChecker networkInfo = DataConnectionChecker();

  Http._internal() {
    dio.interceptors.add(LogInterceptor(responseBody: true)); //debug
    //
    dio.interceptors.add(InterceptorsWrapper(onRequest: (RequestOptions options, RequestInterceptorHandler handler) async {
      return handler.next(options);
    }, onResponse: (diio.Response response, ResponseInterceptorHandler handler) {
      // print("🔥 response: $response}");
      return handler.next(response);
    }, onError: (DioException e, ErrorInterceptorHandler handler) {
      print('🔴🔴🔴 Dio Error：');
      print("e.requestOptions.baseUrl = ${e.requestOptions.baseUrl}");
      print("e.requestOptions.path = ${e.requestOptions.path}");
      print(e);

      if (e.response != null) {
        var response = e.response!;
        var res = BaseResponse.fromJson(response.data);
      }
      return handler.next(e);
    }));
  }

  Future<BaseResponse> get(url, {Map<String, dynamic> param = const {}, String? token}) async {
    _addCommonHeaders(token);
    if (await networkInfo.hasConnection) {
      try {
        diio.Response res = await dio.get(url);
        return BaseResponse.fromJson(jsonDecode(res.data));
      } on DioException catch (e, s) {
        print("DioError======${e.response}");
        if (CancelToken.isCancel(e)) {
          print('Request canceled! ' + e.message!);
        } else if (e.response != null) {
          return _responseFactory(e.response!, url: url);
        }
        return BaseResponse(success: false, message: "");
      } catch (e, s) {
        return BaseResponse(success: false, message: "");
      }
    }
    return BaseResponse(success: false, message: "No Internet connection");
  }

  Future<BaseResponse> post(url, {required Map<String, dynamic> param, String? token}) async {
    ///headers
    _addCommonHeaders(token);
    final formData = FormData.fromMap(param);
    if (await networkInfo.hasConnection) {
      try {
        diio.Response res = await dio.post(url, data: formData);
        return BaseResponse.fromJson(jsonDecode(res.data));
      } on DioException catch (e, s) {
        print("DioError======${e.response}");
        if (CancelToken.isCancel(e)) {
          print('Request canceled! ' + e.message!);
        } else if (e.response != null) {
          return _responseFactory(e.response!, url: url);
        }
        return BaseResponse(success: false, message: "");
      } catch (e, s) {
        return BaseResponse(success: false, message: "");
      }
    } else {
      return BaseResponse(success: false, message: "No Internet connection");
    }
  }

  Future<BaseResponse> delete(url, {String? token}) async {
    ///headers
    _addCommonHeaders(token);
    if (await networkInfo.hasConnection) {
      try {
        diio.Response res = await dio.delete(url);
        return BaseResponse.fromJson(jsonDecode(res.data));
      } on DioException catch (e, s) {
        print("DioError======${e.response}");
        if (CancelToken.isCancel(e)) {
          print('Request canceled! ' + e.message!);
        } else if (e.response != null) {
          return _responseFactory(e.response!, url: url);
        }
        return BaseResponse(success: false, message: "");
      } catch (e, s) {
        return BaseResponse(success: false, message: "");
      }
    } else {
      return BaseResponse(success: false, message: "No Internet connection");
    }
  }

  _addCommonHeaders(String? token) {
    if (token != null) {
      dio.options.headers["Authorization"] = "Bearer $token";
      dio.options.headers = {'Content-Type': 'application/json', 'Accept': 'application/json', "Authorization": "Bearer $token"};
    } else {
      dio.options.headers = {
        'Content-Type': 'application/json',
        'Accept': 'application/json',
      };
    }
  }

  _responseFactory(diio.Response response, {required String url}) {
    BaseResponse res;
    print("response.data");
    print(response.data);
    res = BaseResponse.fromJson(response.data);

    return res;
  }
}

class BaseResponse {
  bool success = false;
  String message = "";
  dynamic data;

  BaseResponse({required this.success, required this.message, this.data = const {}});

  BaseResponse.fromJson(Map<String, dynamic> json) {
    message = json['message'] ?? '';
    success = json['success'];
    data = json['data'] ?? '';
  }
}
