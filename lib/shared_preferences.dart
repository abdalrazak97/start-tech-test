import 'dart:convert';

import 'package:shared_preferences/shared_preferences.dart';

class Preferences {
  static late SharedPreferences preferences;

  static const String keyIsLogged = 'key_IsLogged';
  static const String keyUserToken = 'key_UserToken';
  static const String keyUserId = 'key_UserId';
  static const String keyUserName = 'key_UserName';
  static const String keyUserPhone = 'key_UserPhone';
  static const String keyUserEmail = 'key_UserEmail';

  static init() async {
    preferences = await SharedPreferences.getInstance();
  }

  static Future<void> saveUserTokenFromLocalMemory(String? userToken) async {
    String userTokenEncoded = jsonEncode(userToken);
    preferences.setString(keyUserToken, userTokenEncoded);
  }

  static String getUserTokenFromLocalMemory() {
    String? userToken = preferences.getString(keyUserToken);

    if (null == userToken) {
      return '';
    }
    String userTokenDecoded = jsonDecode(userToken);
    return userTokenDecoded;
  }

  static Future<void> saveUserIdFromLocalMemory(String? id) async {
    String idEncoded = jsonEncode(id);
    preferences.setString(keyUserId, idEncoded);
  }

  static String getUserIdFromLocalMemory() {
    String? id = preferences.getString(keyUserId);

    if (null == id) {
      return '';
    }
    String idDecoded = jsonDecode(id);
    return idDecoded;
  }

  static Future<void> saveUserNameFromLocalMemory(String? name) async {
    String nameEncoded = jsonEncode(name);
    preferences.setString(keyUserName, nameEncoded);
  }

  static String getUserNameFromLocalMemory() {
    String? name = preferences.getString(keyUserName);

    if (null == name) {
      return '';
    }
    String nameDecoded = jsonDecode(name);
    return nameDecoded;
  }


  static Future<void> saveUserPhoneFromLocalMemory(String? phone) async {
    String phoneEncoded = jsonEncode(phone);
    preferences.setString(keyUserPhone, phoneEncoded);
  }

  static String getUserPhoneFromLocalMemory() {
    String? phone = preferences.getString(keyUserPhone);

    if (null == phone) {
      return '';
    }
    String phoneDecoded = jsonDecode(phone);
    return phoneDecoded;
  }

  static Future<void> saveUserEmailFromLocalMemory(String? email) async {
    String emailEncoded = jsonEncode(email);
    preferences.setString(keyUserEmail, emailEncoded);
  }

  static String getUserEmailFromLocalMemory() {
    String? email = preferences.getString(keyUserEmail);

    if (null == email) {
      return '';
    }
    String emailDecoded = jsonDecode(email);
    return emailDecoded;
  }


  static Future<void> saveIsLoggedFromLocalMemory(bool? isLogged) async {
    String isLoggedEncoded = jsonEncode(isLogged.toString());
    preferences.setString(keyIsLogged, isLoggedEncoded);
  }

  static bool getIsLoggedFromLocalMemory() {
    String? isLogged = preferences.getString(keyIsLogged);

    if (null == isLogged) {
      return false;
    }
    isLogged = jsonDecode(isLogged);
    if (isLogged == 'false') {
      return false;
    } else {
      return true;
    }
  }
}
