abstract class AppRoutes {
  static const welcome = '/welcome';
  static const home = '/home';
  static const login = '/login';
  static const register = '/register';

  static const updateInformation = '/home/updateInformation';
}
