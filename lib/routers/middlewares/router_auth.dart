import 'package:flutter/material.dart';
import 'package:get/get.dart';

import '../../config/constant.dart';
import '../../globals.dart';
import '../app_routers.dart';

class RouteAuthMiddleware extends GetMiddleware {
  @override
  int? priority = 0;

  RouteAuthMiddleware({required this.priority});



  @override
  void onPageDispose() {
    print('🍄 Get.currentRoute: ${Get.currentRoute}');
    super.onPageDispose();
  }

  @override
  RouteSettings? redirect(String? route) {

    if (isLogin) {
      return const RouteSettings(name: AppRoutes.home);
    } else {
      return super.redirect(route);
    }
  }
}
