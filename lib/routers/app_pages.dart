import 'package:get/get.dart';
import 'package:start_tech/pages/login/binding.dart';
import 'package:start_tech/pages/login/view.dart';
import 'package:start_tech/pages/welcome/view.dart';

import '../pages/home/binding.dart';
import '../pages/home/view.dart';
import '../pages/register/binding.dart';
import '../pages/register/view.dart';
import '../pages/welcome/binding.dart';
import '../pages/update_information/binding.dart';
import '../pages/update_information/view.dart';
import 'app_routers.dart';
import 'middlewares/router_auth.dart';

class AppPages {
  static const INITIAL = AppRoutes.welcome;

  static final List<GetPage> routes = [
    GetPage(
      name: AppRoutes.welcome,
      page: () => WelcomePage(),
      binding: WelcomeBinding(),
      middlewares: [
        RouteAuthMiddleware(priority: 0)
      ]

    ),
    GetPage(name: AppRoutes.login, page: () => LoginPage(), binding: LoginBinding(), transition: Transition.downToUp),
    GetPage(name: AppRoutes.register, page: () => RegisterPage(), binding: RegisterBinding(), transition: Transition.downToUp),
    GetPage(name: AppRoutes.home, page: () => HomePage(), binding: HomeBinding()),
    GetPage(name: AppRoutes.updateInformation, page: () => UpdateInformationPage(), binding: UpdateInformationBinding()),
  ];
}
