import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:get/get.dart';
import 'package:start_tech/shared_preferences.dart';

import 'config/constant.dart';
import 'config/theme.dart';
import 'globals.dart';
import 'routers/app_pages.dart';

void main() async {
  WidgetsFlutterBinding.ensureInitialized();
  SystemChrome.setPreferredOrientations([
    DeviceOrientation.portraitUp,
  ]);
  await Preferences.init();
  runApp(const MyApp());
}

RouteObserver<PageRoute> routeObserver = RouteObserver<PageRoute>();

class MyApp extends StatefulWidget {
  const MyApp({super.key});

  @override
  State<MyApp> createState() => _MyAppState();
}

class _MyAppState extends State<MyApp> {
  @override
  void initState() {
    isLogin = Preferences.getIsLoggedFromLocalMemory();
    userToken = Preferences.getUserTokenFromLocalMemory();
    id = Preferences.getUserIdFromLocalMemory();
    userName = Preferences.getUserNameFromLocalMemory();
    phone = Preferences.getUserPhoneFromLocalMemory();
    email = Preferences.getUserEmailFromLocalMemory();
    super.initState();
  }
  @override
  Widget build(BuildContext context) {
    return ScreenUtilInit(
        designSize: const Size(360, 690),
        minTextAdapt: true,
        splitScreenMode: true,
        // Use builder only if you need to use library outside ScreenUtilInit context
        builder: (_, child) {
          return GetMaterialApp(
            navigatorKey: NavKey.navKey,
            supportedLocales: const [
              Locale('en'),
            ],
            localeResolutionCallback: (Locale? locale, Iterable<Locale> supportedLocales) {
              return locale;
            },
            theme: Themes.lightTheme,
            debugShowCheckedModeBanner: kDebugMode,
            initialRoute: AppPages.INITIAL,
            getPages: AppPages.routes,
            key: GlobalKey(),
            navigatorObservers: <NavigatorObserver>[routeObserver],
          );
        });
  }
}

class NavKey {
  static final navKey = GlobalKey<NavigatorState>();
}
