// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'user_model.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

UserModelDataModel _$UserModelDataModelFromJson(Map<String, dynamic> json) =>
    UserModelDataModel(
      id: json['id'] as String,
      token: json['token'] as String?,
      name: json['name'] as String?,
      country_code: json['country_code'] as String?,
      phone: json['phone'] as String?,
      email: json['email'] as String?,
    );

Map<String, dynamic> _$UserModelDataModelToJson(UserModelDataModel instance) =>
    <String, dynamic>{
      'id': instance.id,
      'name': instance.name,
      'country_code': instance.country_code,
      'phone': instance.phone,
      'email': instance.email,
      'token': instance.token,
    };
