import 'package:json_annotation/json_annotation.dart';

part 'user_model.g.dart';

@JsonSerializable()
class UserModelDataModel {
  final String id;
  final String? name;
  final String? country_code;
  final String? phone;
  final String? email;
  final String? token;

  UserModelDataModel({
    required this.id,
    this.token,
    this.name,
    this.country_code,
    this.phone,
    this.email,
  });

  Map<String, dynamic> toJson() => _$UserModelDataModelToJson(this);

  factory UserModelDataModel.fromJson(Map<String, dynamic> json) => _$UserModelDataModelFromJson(json);
}
