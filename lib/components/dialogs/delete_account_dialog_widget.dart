import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:get/get.dart';



class DeleteAccountDialogWidget extends StatelessWidget {
  final VoidCallback callback;
  const DeleteAccountDialogWidget({Key? key,required this.callback}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Dialog(
        backgroundColor: Colors.transparent,
        child: Container(
          decoration: BoxDecoration(color: Colors.white, borderRadius: BorderRadius.circular(20.r)),
          child: Column(
            mainAxisSize: MainAxisSize.min,
            children: [
              SizedBox(
                height: 30.h,
              ),
              Center(
                child: Image.asset(
                  "assets/images/info_circle_icon.png",
                  width: 78.r,
                  height: 78.r,
                  color: Theme.of(context).primaryColor,
                ),
              ),
              SizedBox(
                height: 26.h,
              ),
              Padding(
                padding: EdgeInsets.symmetric(horizontal: 12.r),
                child: Text("Are you sure you want to delete your account?",
                    style: Theme.of(context).textTheme.displayLarge,
                textAlign: TextAlign.center,
                ),
              ),
              SizedBox(
                height: 24.h,
              ),
              Padding(
                padding: EdgeInsets.symmetric(horizontal: 30.r),
                child: Row(
                  children: [
                    Expanded(
                      child: InkWell(
                        onTap: () {
                          Get.back();
                        },
                        child: Container(
                          padding: EdgeInsets.symmetric(horizontal: 26.r, vertical: 10.r),
                          alignment: Alignment.center,
                          decoration: BoxDecoration(
                            border: Border.all(width: 1, color: Theme.of(context).primaryColor),
                            borderRadius: BorderRadius.circular(4.r),
                          ),
                          child: Text(
                            "Cancel",
                            style: Theme.of(context).textTheme.displayLarge?.copyWith(
                                  color: Theme.of(context).primaryColor,
                                  fontSize: 12.sp,
                                ),
                          ),
                        ),
                      ),
                    ),
                    SizedBox(
                      width: 16.w,
                    ),
                    Expanded(
                      child: InkWell(
                        onTap: () {
                           callback();
                          Get.back();
                        },
                        child: Container(
                          alignment: Alignment.center,
                          padding: EdgeInsets.symmetric(horizontal: 26.r, vertical: 10.r),
                          decoration: BoxDecoration(
                            color: Theme.of(context).primaryColor,
                            border: Border.all(width: 1, color: Theme.of(context).primaryColor),
                            borderRadius: BorderRadius.circular(4.r),
                          ),
                          child: Text(
                            "Confirm",
                            style: Theme.of(context).textTheme.displayLarge?.copyWith(
                                  color: Colors.white,
                                  fontSize: 12.sp,
                                ),
                          ),
                        ),
                      ),
                    ),
                  ],
                ),
              ),
              SizedBox(
                height: 28.h,
              ),
            ],
          ),
        ));
  }
}
