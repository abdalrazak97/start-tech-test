import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';

class CellWidget extends StatefulWidget {
  final VoidCallback? callback;
  final String title;

  const CellWidget({Key? key, required this.title, this.callback}) : super(key: key);

  @override
  State<CellWidget> createState() => _CellWidgetState();
}

class _CellWidgetState extends State<CellWidget> {
  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: widget.callback,
      child: Card(
        child: Padding(
          padding: EdgeInsets.all( 12.r),
          child: Row(mainAxisAlignment: MainAxisAlignment.spaceBetween, children: [
            Text(
              widget.title,
              style: Theme.of(context).textTheme.displayLarge?.copyWith(fontSize: 18.sp),
            ),
            Icon(
              Icons.navigate_next,
              size: 25.r,
              color: Theme.of(context).primaryColor,
            ),
          ]),
        ),
      ),
    );
  }
}
