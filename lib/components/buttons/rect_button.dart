import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:flutter/material.dart';

class RectButton extends StatefulWidget {
  final Widget? child;
  final String title;
  final double? width;
  final double? height;
  final Color? textColor;
  final Color? backgroundColor;
  final double? fontSize;
  final FontWeight? fontWeight;
  final Color? borderColor;
  final VoidCallback? onPressed;
  final bool enable;
  final bool isFullDisplay;

  const RectButton(
      {Key? key,
      this.title = "",
      this.width,
      this.height,
      this.child,
      this.textColor,
      this.backgroundColor,
      this.fontSize,
      this.fontWeight,
      this.borderColor,
      this.onPressed,
      this.enable = true,
      this.isFullDisplay = true})
      : super(key: key);

  @override
  State createState() => _RectButtonState();
}

class _RectButtonState extends State<RectButton> {
  @override
  Widget build(BuildContext context) {
    return ElevatedButton(
        onPressed: !widget.enable || widget.onPressed == null ? null : () => widget.onPressed!(),
        style: ButtonStyle(
            splashFactory: NoSplash.splashFactory,
            shadowColor: MaterialStateProperty.all<Color>(Colors.transparent),
            padding: MaterialStateProperty.all<EdgeInsetsGeometry>(EdgeInsets.zero),
            side: widget.borderColor == null
                ? MaterialStateProperty.all<BorderSide>(BorderSide.none)
                : MaterialStateProperty.all<BorderSide>(BorderSide(color: widget.borderColor!)),
            minimumSize: MaterialStateProperty.all<Size>(Size(widget.width ?? double.infinity, widget.height ?? 44.h)),
            maximumSize: MaterialStateProperty.all<Size>(Size(widget.width ?? double.infinity, widget.height ?? 44.h)),
            backgroundColor: MaterialStateProperty.all<Color>(widget.backgroundColor ?? Theme.of(context).primaryColor),
            shape: MaterialStateProperty.all<RoundedRectangleBorder>(
              RoundedRectangleBorder(borderRadius: BorderRadius.circular(widget.height != null ? widget.height! / 2.0 : 10.r)),
            )),
        child: widget.child ??
            Text(widget.title,
                style:  TextStyle(
                    fontFamily: "AlexandriaFLF",
                    backgroundColor: Colors.transparent,
                    fontSize: widget.fontSize ?? 14,
                    fontWeight: widget.fontWeight ?? FontWeight.w500,
                    color: widget.textColor ?? Theme.of(context).textTheme.displayLarge?.color)));
  }
}
