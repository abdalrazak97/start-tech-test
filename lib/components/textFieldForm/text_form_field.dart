import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';

import '../../config/theme.dart';

class TextFormFieldWidget extends StatelessWidget {
  const TextFormFieldWidget({
    super.key,
    this.controller,
    this.hintText,
    this.hintColor,
    this.helpText,
    this.prefixIcon,
    this.suffix,
    this.isPassword,
    this.enabled,
    this.readOnly,
    this.borderColor,
    this.textColor,
    this.isDense,
    this.labelText,
    this.labelColor,
    this.focusBorderColor,
    this.toggleObscured,
    this.textInputType,
    this.regExp,
    this.maxLength,
    this.onTab,
    this.onChange,
    this.suffixIcon,
    this.onFieldSubmitted,
    this.textInputAction,
    this.suffixCallBack,
    this.title,
    this.validator,
    this.textAlign,
  });

  final TextEditingController? controller;
  final TextAlign? textAlign;
  final String? hintText, helpText, labelText, title;
  final Widget? prefixIcon, suffixIcon;
  final bool? suffix;
  final bool? isPassword, enabled, readOnly, isDense;
  final Color? borderColor, focusBorderColor;
  final Color? textColor, hintColor, labelColor;
  final VoidCallback? toggleObscured, suffixCallBack;
  final TextInputType? textInputType;
  final RegExp? regExp;
  final TextInputAction? textInputAction;
  final VoidCallback? onTab;
  final int? maxLength;
  final ValueChanged<String>? onChange, onFieldSubmitted;
  final String? Function(String?)? validator;

  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        if (title != null)
          Padding(
            padding: EdgeInsets.only(bottom: 10.h),
            child: Text(
              title!,
              style: Theme.of(context).textTheme.displayMedium,
            ),
          ),
        TextFormField(
          textAlign: textAlign ?? TextAlign.center,
          onChanged: onChange,
          onTap: onTab,
          validator: validator,
          onFieldSubmitted: onFieldSubmitted,
          controller: controller,
          maxLength: maxLength,
          inputFormatters: <TextInputFormatter>[
            if (textInputType != null && textInputType == TextInputType.numberWithOptions(signed: true)) FilteringTextInputFormatter.digitsOnly
          ],
          keyboardType: textInputType ?? TextInputType.text,
          readOnly: readOnly ?? false,
          obscureText: isPassword ?? false,
          textInputAction: textInputAction,
          style: TextStyle(color: textColor ?? Colors.black, fontSize: 15.sp),
          decoration: InputDecoration(
            filled: true,
            fillColor: Colors.white,
            isDense: isDense,
            contentPadding: EdgeInsets.zero,
            counterStyle: TextStyle(color: hintColor ?? Theme.of(context).hintColor, fontSize: 13.sp),
            focusedBorder: OutlineInputBorder(
              borderRadius: BorderRadius.circular(10.0.w),
              borderSide: BorderSide(
                color: focusBorderColor ?? Colors.black,
                width: 1.0.h,
              ),
            ),
            enabledBorder: OutlineInputBorder(
              borderRadius: BorderRadius.circular(10.0.w),
              borderSide: BorderSide(
                color: MyColors.greyTextColor,
                width: 1.0.h,
              ),
            ),
            disabledBorder: OutlineInputBorder(
                borderRadius: BorderRadius.circular(10.0.w), borderSide: BorderSide(width: 1, color: borderColor ?? MyColors.greyTextColor)),
            border: OutlineInputBorder(
              borderRadius: BorderRadius.circular(10.0.w),
              borderSide: BorderSide(
                color: borderColor ?? MyColors.greyTextColor,
                width: 1.0.h,
              ),
            ),
            hintText: hintText ?? '',
            hintStyle: TextStyle(
              color: hintColor ?? MyColors.greyTextColor,
              fontSize: 15.sp,
            ),
            helperText: helpText ?? '',
            prefixIcon: prefixIcon,
            suffix: null == suffix
                ? null
                : Padding(
                    padding: EdgeInsets.symmetric(
                      horizontal: 6.w,
                    ),
                    child: SizedBox(
                      height: 22.h,
                      child: GestureDetector(
                        onTap: toggleObscured,
                        child: Icon(
                          isPassword ?? false ? Icons.visibility_rounded : Icons.visibility_off_rounded,
                          size: 20.w,
                          color: MyColors.greyTextColor,
                        ),
                      ),
                    ),
                  ),
            suffixIcon: null == suffixIcon
                ? null
                : GestureDetector(
                    onTap: suffixCallBack,
                    child: Padding(
                      padding: EdgeInsets.all(10.sp),
                      child: Container(
                        decoration: const BoxDecoration(color: Colors.red, shape: BoxShape.circle),
                        child: Padding(
                            padding: EdgeInsets.all(0.sp),
                            child: Icon(
                              Icons.close,
                              size: 16.sp,
                              color: Colors.white,
                            )),
                      ),
                    ),
                  ),
            enabled: enabled ?? true,
            labelText: labelText,
            labelStyle: TextStyle(
              color: labelColor ?? MyColors.greyTextColor,
            ),
          ),
        ),
      ],
    );
  }
}
